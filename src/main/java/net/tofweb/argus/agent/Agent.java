package net.tofweb.argus.agent;

import java.io.IOException;

import org.apache.commons.cli.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.tofweb.argus.domain.TokenRequest;
import net.tofweb.argus.domain.model.AgentModel;

public class Agent {
	
	protected static final Logger log = LogManager.getLogger();
	
	public void run(String[] args) throws ParseException, ClientProtocolException, IOException {
		CommandLineProcessor commandLineProcessor = new CommandLineProcessor();
		ArgusCommand argusCommand = commandLineProcessor.parseCommand(args);
		argusCommand.validate();
		
		TokenRequest argusRequest = new TokenRequest(new AgentModel(), argusCommand.getFiles());
		
		ArgusClient client = new ArgusClient();
		String agentToken = client.requestAgentToken(argusRequest);
		
		log.debug("Agent token is: " + agentToken);
	}
}
