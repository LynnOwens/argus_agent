package net.tofweb.argus.agent;

import java.io.File;
import java.util.Arrays;

public class ArgusCommand {
	private File[] files;

	public void setFile(String pathname) {
		if (pathname == null) {
			throw new IllegalArgumentException("Pathname may not be null");
		}
		
		File file = new File(pathname);
		File[] files = new File[1];
		files[0] = file;
		
		this.files = files;
	}

	public void validate() { 
		Arrays.stream(files).forEach(file -> file.exists());
	}
	
	public File[] getFiles() {
		return this.files;
	}

}
