package net.tofweb.argus.agent;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CommandLineProcessor {
	
	protected static final Logger log = LogManager.getLogger();
	
	private Options options;
	private CommandLineParser commandLineParser;
	
	public static final String OPT_PATHNAME = "f";
	
	public CommandLineProcessor() {
		options = buildOptions();
		commandLineParser = new DefaultParser();
	}
	
	private Options buildOptions() {
		Options options = new Options();
		options.addOption(Option.builder(OPT_PATHNAME).longOpt("file").hasArg(true).desc("File pathname to stream to server").build());
		return options;
	}

	public ArgusCommand parseCommand(String[] args) throws ParseException {
		CommandLine cmd = commandLineParser.parse(options, args);
		
		ArgusCommand argusCommand = new ArgusCommand();
		argusCommand.setFile(cmd.getOptionValue(OPT_PATHNAME));
		return argusCommand;
	}

	public Options getOptions() {
		return options;
	}
}
