package net.tofweb.argus.agent;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;

import net.tofweb.argus.domain.TokenRequest;
import net.tofweb.argus.domain.TokenResponse;

public class ArgusClient {

	public String requestAgentToken(TokenRequest tokenRequest) throws ClientProtocolException, IOException  {
		Gson gson = new Gson();
		String requestJson = gson.toJson(tokenRequest);
		HttpEntity requestEntity = new StringEntity(requestJson);
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost("http://localhost/request");
		httpPost.setEntity(requestEntity);
		CloseableHttpResponse response = httpclient.execute(httpPost);

		HttpEntity responseEntity;
		
		try {
		    responseEntity = response.getEntity();
		    // do something useful with the response body
		    // and ensure it is fully consumed
		    EntityUtils.consume(responseEntity);
		} finally {
		    response.close();
		}
		
		String responseString = EntityUtils.toString(responseEntity, "UTF-8");
		TokenResponse tokenResponse = gson.fromJson(responseString, TokenResponse.class);
		
		return tokenResponse.getAgentToken();
	}


}
