package net.tofweb.argus.agent;

import java.io.IOException;

import org.apache.commons.cli.ParseException;
import org.apache.http.client.ClientProtocolException;

public class Main {
	public static void main(String[] args) throws ParseException, ClientProtocolException, IOException {
		Agent a = new Agent();
		a.run(args);
	}
}
