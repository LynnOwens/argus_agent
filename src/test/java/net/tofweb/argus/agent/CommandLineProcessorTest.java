package net.tofweb.argus.agent;

import static org.junit.jupiter.api.Assertions.*;

import org.apache.commons.cli.ParseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CommandLineProcessorTest {
	
	private CommandLineProcessor processor;
	
	@BeforeEach
	void beforeEach() {
		processor = new CommandLineProcessor();
	}

	@Test
	void testFoo() throws ParseException {
		assertThrows(IllegalArgumentException.class, () -> {
			processor.parseCommand(new String[]{ "foo" });
		});
	}

	@Test
	void testF() throws ParseException {
		assertThrows(IllegalArgumentException.class, () -> {
			processor.parseCommand(new String[]{ "f" });
		});
	}
	
	@Test
	void testFfoo() throws ParseException {
		assertThrows(IllegalArgumentException.class, () -> {
			processor.parseCommand(new String[]{ "f", "foo" });
		});
	}
	
	@Test
	void testDashFfoo() throws ParseException {
		ArgusCommand cmd = processor.parseCommand(new String[]{ "-f", "foo" });
		assertTrue(cmd.getFiles().length == 1);
	}
	
	@Test
	void testDashF() throws ParseException {
		assertThrows(IllegalArgumentException.class, () -> {
			processor.parseCommand(new String[]{ "f" });
		});
	}
}
